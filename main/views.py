from django.http.response import HttpResponse, JsonResponse, Http404

from .models import *
# Create your views here.


def home(request):
    return HttpResponse('''<html>
    <head><title>kasoho.com</title>
    </head>
    <body bgcolor="#06163C">
        <font face="ubuntu" size=20><p align=center style="color:#E0DEDC">
            This is the front page ! :D
        </p></font>
    </body>
</html>''')


def catagory(request, cat):
    cats = Catagory.objects.filter(name=cat)
    if len(cats) == 0:
        raise Http404("Category '{}' does not exist".format(cat))

    return JsonResponse({
        'catagory': [obj.name for obj in cats],
        'roots': [obj.slug for obj in Root.objects.
                    filter(catagory=Catagory.objects.filter(name=cat))]
        }, safe=False)


def root(request, root):
    roots = Root.objects.filter(slug=root)

    try:
        return JsonResponse({
            'roots': [obj.slug for obj in roots],
            'process': [obj.name for obj in Process.objects.filter(
                    root=Root.objects.get(slug=root))]
            }, safe=False)
    except:
        raise Http404("Root '{}' does not exist".format(root))
