from django.db import models

# Create your models here.


class Catagory(models.Model):
    name = models.CharField(max_length=20)
    slug = models.SlugField(unique=True, max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "catagories"


class Root(models.Model):
    name = models.CharField(max_length=100)
    catagory = models.ForeignKey(Catagory, default=None)
    slug = models.SlugField(unique=True, max_length=50)

    def __str__(self):
        return self.name


class Info(models.Model):
    name = models.CharField(name="Office Name", max_length=100)
    latitude = models.FloatField()
    altitude = models.FloatField()
    root = models.ForeignKey(Root)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Office Information"


class Process(models.Model):
    step_no = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    content = models.TextField()
    root = models.ForeignKey(Root)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Step-by-Step Process"
