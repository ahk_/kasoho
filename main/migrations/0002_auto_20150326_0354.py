# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Info',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('info', models.TextField()),
                ('latitude', models.FloatField()),
                ('altitude', models.FloatField()),
                ('root', models.ForeignKey(to='main.Root')),
            ],
            options={
                'verbose_name_plural': 'Info',
            },
            bases=(models.Model,),
        ),
        migrations.RenameModel(
            old_name='Data',
            new_name='Process',
        ),
        migrations.AlterModelOptions(
            name='process',
            options={'verbose_name_plural': 'Step-by-Step Process'},
        ),
    ]
