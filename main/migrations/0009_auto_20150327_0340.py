# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0008_auto_20150327_0339'),
    ]

    operations = [
        migrations.AlterField(
            model_name='root',
            name='catagory',
            field=models.ForeignKey(default=None, to='main.Catagory'),
            preserve_default=True,
        ),
    ]
