# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_auto_20150326_0756'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='info',
            options={'verbose_name_plural': 'Information'},
        ),
        migrations.RenameField(
            model_name='info',
            old_name='name',
            new_name='Office Name',
        ),
        migrations.RemoveField(
            model_name='info',
            name='info',
        ),
    ]
