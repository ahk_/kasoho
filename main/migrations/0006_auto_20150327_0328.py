# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_auto_20150326_1801'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='info',
            options={'verbose_name_plural': 'Office Information'},
        ),
        migrations.RemoveField(
            model_name='process',
            name='id',
        ),
        migrations.AddField(
            model_name='process',
            name='Step Number',
            field=models.AutoField(default=1, serialize=False, primary_key=True),
            preserve_default=False,
        ),
    ]
