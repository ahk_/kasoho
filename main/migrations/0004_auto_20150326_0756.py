# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_auto_20150326_0419'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='catagory',
            options={'verbose_name_plural': 'catagories'},
        ),
        migrations.AddField(
            model_name='root',
            name='slug',
            field=models.SlugField(default=datetime.datetime(2015, 3, 26, 7, 56, 23, 980764, tzinfo=utc), unique=True),
            preserve_default=False,
        ),
    ]
