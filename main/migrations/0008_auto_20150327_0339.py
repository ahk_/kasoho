# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0007_auto_20150327_0331'),
    ]

    operations = [
        migrations.AlterField(
            model_name='process',
            name='step_no',
            field=models.AutoField(default=1, serialize=False, primary_key=True),
            preserve_default=True,
        ),
    ]
