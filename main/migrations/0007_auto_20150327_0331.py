# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0006_auto_20150327_0328'),
    ]

    operations = [
        migrations.RenameField(
            model_name='process',
            old_name='Step Number',
            new_name='step_no',
        ),
    ]
