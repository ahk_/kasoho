from django.contrib import admin

from .models import Root, Process, Info, Catagory

# Register your models here.


class InfoInLine(admin.TabularInline):
    model = Info
    extra = 1


class ProcessInLine(admin.TabularInline):
    model = Process
    extra = 1


class CatagoryAdmin(admin.ModelAdmin):
    model = Catagory
    prepopulated_fields = {'slug': ('name',)}


class RootAdmin(admin.ModelAdmin):
    inlines = [
            ProcessInLine,
            InfoInLine
            ]
    search_fields = ['name']
    prepopulated_fields = {'slug': ('name',)}

admin.site.register(Root, RootAdmin)
admin.site.register(Catagory, CatagoryAdmin)
